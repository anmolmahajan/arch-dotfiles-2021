set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec --no-startup-id termite
bindsym $mod+Shift+Return exec --no-startup-id termite -t "console"

# floating windows
for_window [class="Tk"] floating enable
for_window [class="mpv"] floating enable
for_window [class=Polybar] floating enable
for_window [class="Baobab"] floating enable
for_window [class="Lxappearance"] floating enable  
for_window [class="Termite" title="^neofetch*"] floating enable

# kill focused window
bindsym $mod+q kill
bindsym Mod1+F4 kill

# rofi
bindsym $mod+d exec --no-startup-id "~/.config/rofi/bin/launcher"

# Constants
set $mod Mod4
set $workspace1 "1:" 
set $workspace2 "2:" 
set $workspace3 "3:" 
set $workspace4 "4:" 
set $workspace5 "5:" 
set $workspace6 "6:"

#Assign Workspace to Monitor
workspace $workspace1 output primary
workspace $workspace2 output primary
workspace $workspace3 output primary
workspace $workspace4 output HDMI-0
workspace $workspace5 output HDMI-0
workspace $workspace6 output primary
  

#Rofi keybinds
bindsym $mod+x exec --no-startup-id "~/.config/rofi/bin/powermenu-i3"
bindsym $mod+n exec --no-startup-id networkmanager_dmenu
bindsym $mod+Shift+s exec --no-startup-id "~/.config/rofi/bin/screenshot"


# Start applications
bindsym Shift+Print exec --no-startup-id scrot '%Y-%m-%d-%H-%M-%S_scrot.png' -q 100
bindsym $mod+Shift+f exec --no-startup-id chromium
bindsym $mod+Shift+t exec --no-startup-id thunar
bindsym $mod+Mod1+s exec --no-startup-id "spotify --force-device-scale-factor=1.000001"
bindsym $mod+Shift+v exec --no-startup-id code
bindsym $mod+Shift+b exec --no-startup-id bleachbit

# Program Workspaces
assign [class="Termite" title="^console*"] $workspace5
assign [class="Chromium"] $workspace2
assign [class="Thunar"] $workspace3
assign [class="Code"] $workspace4
assign [class="factorio"] $workspace6
assign [class="Slack"] $workspace6
for_window [class="Spotify"] move to workspace $workspace5

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle
bindsym $mod+Mod1+space sticky toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace $workspace1 
bindsym $mod+2 workspace $workspace2 
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4 
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
bindsym $mod+Shift+Mod1+r exec --no-startup-id polybar-msg cmd restart
bindsym $mod+c exec --no-startup-id "bash -c 'if ! pkill -x compton; then exec compton -C; fi'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
set $bg-color          #00000000
set $inactive-bg-color   #00000000
set $text-color          #F3F4F5
set $inactive-text-color #7B8DB3
set $urgent-text-color   #FC524F

# Gaps
gaps inner 7
gaps outer 7
#smart_gaps on

# Colors
set $FOCUS #F9F7F3
set $UNFOCUS #59545D
set $BORDERF #F9F7F3
set $BORDERU #59545D
client.focused 		  	$color12    $BORDERF	$FOCUS		$BORDERF
client.focused_inactive		$color12    $UNFOCUS	$UNFOCUS	$UNFOCUS
client.unfocused		$color4     $BORDERU	$UNFOCUS	$BORDERU
client.urgent			$color5     $FOCUS	$FOCUS		$FOCUS
client.background		$UNFOCUS

# Border size
for_window [class="^.*"] border pixel 1
for_window [class="Tk"] border pixel 0
for_window [class="mpv"] border pixel 0
for_window [class="Polybar"] border pixel 0
for_window [class="Tk"] sticky enable

#Scartchpad
bindsym $mod+minus scratchpad show
bindsym $mod+Shift+minus move scratchpad

# Brightness control
bindsym XF86MonBrightnessUp exec --no-startup-id backlight-up
bindsym XF86MonBrightnessDown exec --no-startup-id backlight-down

# Start-up shit
exec --no-startup-id compton -C
exec --no-startup-id compton -C
exec --no-startup-id mpd
exec --no-startup-id xfsettingsd
exec --no-startup-id xfce4-power-manager
exec --no-startup-id slack
exec --no-startup-id nitrogen --restore
exec --no-startup-id dunst
exec --no-startup-id "/home/anmol/.config/polybar/default-i3/launch.sh"


# Volume control
bindsym XF86AudioLowerVolume exec --no-startup-id volume-down
bindsym XF86AudioRaiseVolume exec --no-startup-id volume-up
bindsym XF86AudioMute exec --no-startup-id amixer set Master toggle

# Media controls
bindsym XF86AudioPlay exec --no-startup-id "playerctl play-pause;mpc toggle;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
bindsym XF86AudioNext exec --no-startup-id "playerctl next; mpc next;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"
bindsym XF86AudioPrev exec --no-startup-id "playerctl previous;mpc prev;dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"

# Tearing fix
new_window 1pixel

#Full Screen Application
for_window [class="factorio"] fullscreen enable
